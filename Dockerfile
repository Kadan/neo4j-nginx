FROM centos
LABEL maintainer="kadan.li <Kadanli@1984@126.com>"
RUN yum install -y java-1.8.0-openjdk java-1.8.0-openjdk-devel
RUN yum install -y wget proc-devel net-tools gcc zlib zlib-devel make openssl-devel
RUN wget http://nginx.org/download/nginx-1.9.7.tar.gz
RUN tar zxvf nginx-1.9.7.tar.gz
WORKDIR nginx-1.9.7
RUN ./configure --prefix=/usr/local/nginx && make && make install
EXPOSE 80
EXPOSE 443
EXPOSE 7474
EXPOSE 7473
RUN echo "daemon off;">>/usr/local/nginx/conf/nginx.conf
WORKDIR /opt/nginx
ADD neo4j-community-3.5.4 /root/neo4j/
ADD startup.sh /root/startup.sh
RUN chmod +x /root/startup.sh
CMD ["/root/startup.sh"]
